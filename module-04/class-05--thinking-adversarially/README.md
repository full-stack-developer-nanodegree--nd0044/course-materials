# Access and Authorization

## [01. Lesson Intro](https://classroom.udacity.com/nanodegrees/nd0044/parts/b91edf5c-5a4d-499a-ba69-a598afd9fe3e/modules/5606de9d-aa2b-4a1b-9b14-81b87d80a264/lessons/2de7e6db-25b4-42bd-b0a6-5a2ba119cff1/concepts/2f9a0323-bb7d-4688-ab47-f86c74cd122d)

### Applying your Knowledge

[![](https://img.youtube.com/vi/p0MivDrG-_c/0.jpg)](https://youtu.be/p0MivDrG-_c)

## [02. Limiting Access to Code and Systems](https://classroom.udacity.com/nanodegrees/nd0044/parts/b91edf5c-5a4d-499a-ba69-a598afd9fe3e/modules/5606de9d-aa2b-4a1b-9b14-81b87d80a264/lessons/2de7e6db-25b4-42bd-b0a6-5a2ba119cff1/concepts/05ac18a0-c9cf-4a03-a497-5f8729e6faa3)

### Using Git Effectively

[![](https://img.youtube.com/vi/cBIWkoo0GMU/0.jpg)](https://youtu.be/cBIWkoo0GMU)

#### What I've learned from here

- Environments
- Different branches when developing
    - master: production ready branch
    - staging: mirrors master branch, tests are performed using fake data and a enviroment close to production
    - dev: development branch
    - hotfixes: checkouts from master, breaking bugs are fixed here
    - features: checkouts from dev, new features are implemented here

### Configuring GitHub with Restrictions

[![](https://img.youtube.com/vi/I2kwsX-B8go/0.jpg)](https://youtu.be/I2kwsX-B8go)

#### What I've learned from here

- To protect branches with rules

### Questions

![](./images/concept-2.question-1.png)

## [03. Code Review for Security](https://classroom.udacity.com/nanodegrees/nd0044/parts/b91edf5c-5a4d-499a-ba69-a598afd9fe3e/modules/5606de9d-aa2b-4a1b-9b14-81b87d80a264/lessons/2de7e6db-25b4-42bd-b0a6-5a2ba119cff1/concepts/cc01bd6a-fef0-44fc-a787-cc9c621baa11)

### Code Review as a Security Tool

[![](https://img.youtube.com/vi/VHh6Wkc6HGQ/0.jpg)](https://youtu.be/VHh6Wkc6HGQ)

#### What I've learned from here

- Use Code review system as a another level of security within our codebase

### Questions

![](./images/concept-3.question-1.png)

### Additional Reading:

- [Palantir Blog on Code Review](https://medium.com/palantir/code-review-best-practices-19e02780015f)
- [AWS IAM Best Practices](https://docs.aws.amazon.com/IAM/latest/UserGuide/best-practices.html)

## [04. Auth Validation Testing](https://classroom.udacity.com/nanodegrees/nd0044/parts/b91edf5c-5a4d-499a-ba69-a598afd9fe3e/modules/5606de9d-aa2b-4a1b-9b14-81b87d80a264/lessons/2de7e6db-25b4-42bd-b0a6-5a2ba119cff1/concepts/ef8a7356-63d8-4d36-9518-64d5bd2ececc)

### Postman Integration Testing

[![](https://img.youtube.com/vi/sfF9jXKjHiA/0.jpg)](https://youtu.be/sfF9jXKjHiA)

#### What I've learned from here

- Use automated tests using Postman ([Test scripts](https://learning.postman.com/docs/postman/scripts/test-scripts/)) for securing and keeping free of bugs

#### Try it yourself!

Write some integration tests using Postman for the server we've been working on throughout the course.

```javascript
pm.test("Test Status is 200", function () {
    pm.response.to.be.ok;
})

pm.test("Test Body is Access Granted", function () {
    pm.response.to.be.withBody;
    var textData = pm.response.text();
    pm.expect(textData).to.be.equal("Access Granted");
})
```

### Penetration Testing

[![](https://img.youtube.com/vi/Lb5fAf2nIbw/0.jpg)](https://youtu.be/Lb5fAf2nIbw)

#### What I've learned from here

- Hire a friendly hacker to attack and find vulnerabilities, this is Penetration testing

### Additional Resources

- [Postman Command Line Interface for Continuous Integration and Delivery](https://learning.getpostman.com/docs/postman/collection_runs/command_line_integration_with_newman/)
- [Lock Picking Lawyer](https://www.youtube.com/channel/UCm9K6rby98W8JigLoZOh6FQ) YouTuber dedicated to demonstrating security - capabilities of common physical locks.
- [Kitboga](https://www.youtube.com/channel/UCm22FAXZMw1BaWeFszZxUKw) A YouTuber who engages with social attacks.

## [05. Alternate Attack Vectors](https://classroom.udacity.com/nanodegrees/nd0044/parts/b91edf5c-5a4d-499a-ba69-a598afd9fe3e/modules/5606de9d-aa2b-4a1b-9b14-81b87d80a264/lessons/2de7e6db-25b4-42bd-b0a6-5a2ba119cff1/concepts/c5659982-2551-4e7b-a584-986e3ea14b39)

### Phishing

[![](https://img.youtube.com/vi/6468qzutFOw/0.jpg)](https://youtu.be/6468qzutFOw)

#### What I've learned from here

- There are very common phishing attacks like using weird domains, like Gmail, and even spoofing some trusted domain like auth.udacity.attack.co
- There are also very sophisticated attacks targetting young students that might have loand, telling them that they could entirelly get rid of their debts
- There are also sophisticated attacks targetting Android systems using illegitimate apps like BTCTurk Pro Beta, that uses features like mining bitcoins to attack their users. There also attacks that could intercept an SMS by reading the notification bar.

### Social Engineering

[![](https://img.youtube.com/vi/9iAaj4V48hs/0.jpg)](https://youtu.be/9iAaj4V48hs)

#### What I've learned from here

- It's really easy to us to self-expose sensitive informations to social medias, even at work. Posting a photo or video at work, a photo or video at home exposing things that could be the source of our secret passwords, then someone just have to scrap our social media to make a database list of possible keywords used to create our passwords.

### Additional Resources

- [Phishing Malicious Apps to Bypass MFA](https://www.welivesecurity.com/2019/06/17/malware-google-permissions-2fa-bypass/)
- [Wikipedia Article on Phishing](https://en.wikipedia.org/wiki/Phishing) - A great resource of history and some common attacks

## [06. Staying Ahead of the Attackers](https://classroom.udacity.com/nanodegrees/nd0044/parts/b91edf5c-5a4d-499a-ba69-a598afd9fe3e/modules/5606de9d-aa2b-4a1b-9b14-81b87d80a264/lessons/2de7e6db-25b4-42bd-b0a6-5a2ba119cff1/concepts/5f252e42-521c-4385-92ed-93309c4e1345)

### Reading the News

[![](https://img.youtube.com/vi/WhZGSYykC6w/0.jpg)](https://youtu.be/WhZGSYykC6w)

#### What I've learned from here

- Keep up to date with security concerns and news, e.g.: on Google News

### OWASP Top 10

[![](https://img.youtube.com/vi/WhZGSYykC6w/0.jpg)](https://youtu.be/WhZGSYykC6w)

### Subscribing to Vulnerability Monitoring

[![](https://img.youtube.com/vi/fIVlNHJRGPc/0.jpg)](https://youtu.be/fIVlNHJRGPc)

#### What I've learned from here

- It's really, really difficult to stay ahead of attackers, to identify all the vulnerabilities within our systems today, and even keep track of possible vulnerabilites in the future.
- More difficult yet it's to continuously ask our vendors the questions presented by the course instructor, Gabriel Ruttner, because is endless and the effort needed for this is impossible.

### Additional Resources

- [Wild West Hackin' Fest](https://www.youtube.com/watch?v=mG87meKVrzc)
- [SOC 2 Compliance](https://en.wikipedia.org/wiki/SSAE_16)

## [07. Course Recap](https://classroom.udacity.com/nanodegrees/nd0044/parts/b91edf5c-5a4d-499a-ba69-a598afd9fe3e/modules/5606de9d-aa2b-4a1b-9b14-81b87d80a264/lessons/2de7e6db-25b4-42bd-b0a6-5a2ba119cff1/concepts/04ae6c75-639d-4df1-8614-2bd1502406fe)

### Identity and Access Management!

[![](https://img.youtube.com/vi/zl1C3WnfzF8/0.jpg)](https://youtu.be/zl1C3WnfzF8)

#### What I've learned from here

- Identifying who is making requests to our services.
- Implemented using third-party authentication plataform (auth0)
- Explored some common techniques that hackers use to thwart plan text passwords
- Understanding how we can mitigate against such attacks
- Implemented role-based permission controls, using third-party auth (auth0)
- Learned that we should be continuously evolving as developers to keep our systems and services secure and stay ahead of attackers